package m2dl.corbarmi;

import org.jacorb.naming.NameServer;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

/**
 * Created by franck on 25/02/2016.
 */
public class NameServiceLaunch {

    static public void main(String[] args) throws IOException {
        String[] args2 = {"-Dorg.omg.CORBA.ORBClass=org.jacorb.orb.ORB","-Dorg.omg.CORBA.ORBSingletonClass=org.jacorb.orb.ORBSingleton","-DOAPort=1200"};
        NameServer.main(args2);
    }

}
