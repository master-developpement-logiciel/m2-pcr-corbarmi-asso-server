package m2dl.corbarmi.asso;

import servAsso.AssoServerPOA;

import java.util.HashMap;

/**
 * Created by franck on 25/02/2016.
 */
class AssoServerImpl extends AssoServerPOA {

	class Asso {
		public String id;
		public String cp;
		public String name;
		public String descr;
		public Asso(String id, String name, String cp, String descr) {
			super();
			this.id = id;
			this.name = name;
			this.cp = cp;
			this.descr = descr;
		}
	}

	private HashMap<String, Asso> assos = new HashMap<String, Asso>();

	private void addAsso(String id, String name, String cp, String descr) {
		assos.put(id, new Asso(id, name, cp, descr));
	}

	public AssoServerImpl() {
		addAsso("1", "Lala", "31000", "lala est super");
		addAsso("2", "Ola", "72000", "que tal ?");
	}

	public String[] getAssoIDs() {
		return assos.keySet().toArray(new String[0]);
	}

	public String getAssoCP(String assoID) {
		return assos.get(assoID).cp;
	}

	public String getAssoName(String assoID) {
		return assos.get(assoID).name;
	}

	public String getDescription(String assoID) {
		return assos.get(assoID).descr;
	}
}
