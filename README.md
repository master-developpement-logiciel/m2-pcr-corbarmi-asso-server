M2 DL - TP PCR - Corba/RMI
=======================

Ce projet est un projet Maven permettant de lancer le service Corba "AssoServer" permettant d'accèder aux informations sur les associations liées à la recherche introduite en TP.

----------

### 1. Compiler le projet
Exécuter la commande maven
mvn compile

### 2. Lancer le "name service"
Exécuter la commande Maven :
mvn exec:java -Dexec.mainClass=m2dl.corbarmi.NameServiceLaunch

### 3. Lancer le "asso service"
Exécuter la commande Maven :
mvn exec:java -Dexec.mainClass=m2dl.corbarmi.asso.AssoServerLaunch



