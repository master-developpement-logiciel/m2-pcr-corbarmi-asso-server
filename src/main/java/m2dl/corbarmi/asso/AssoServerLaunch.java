package m2dl.corbarmi.asso;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import servAsso.AssoServer;
import servAsso.AssoServerHelper;

public class AssoServerLaunch {
	
	public static void main(String args[]) throws InvalidName, AdapterInactive, ServantNotActive, WrongPolicy, org.omg.CosNaming.NamingContextPackage.InvalidName, NotFound, CannotProceed {

        //String[] args2 = {"-ORBInitialPort", "1050", "-ORBInitialHost", "127.0.0.1"};
        java.util.Properties props = new java.util.Properties();

        props.put("org.omg.CORBA.ORBClass", "org.jacorb.orb.ORB");
        props.put("org.omg.CORBA.ORBSingletonClass", "org.jacorb.orb.ORBSingleton");
        props.put("ORBInitRef.NameService","corbaloc::127.0.0.1:1200/StandardNS/NameServer-POA/_");

        props.put("ORBInitialPort","1050");
        props.put("ORBInitialHost","127.0.0.1");

        ORB orb = ORB.init(args,props);
		
		POA root = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
		root.the_POAManager().activate();
		
		AssoServerImpl assoServerImpl = new AssoServerImpl();
		
		org.omg.CORBA.Object ref = root.servant_to_reference(assoServerImpl);
		AssoServer asRef = AssoServerHelper.narrow(ref);
		
		org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
		
		NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
		
		NameComponent path[] = ncRef.to_name("assoServer");
		ncRef.rebind(path, asRef);
		
		System.out.println("NameService and AssoServer ready and waiting ...");

		// wait for invocations from clients
		orb.run();
		
	}
	
}